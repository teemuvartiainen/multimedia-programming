 // Polyline is a basic data container for the polyline being drawn
function myPolyline(x,y) {
  this.startX;
  this.startY;
  this.time;
  this.points = [];
  this.offsetX = x ? x : 0;
  this.offsetY = y ? y : 0;
  this.linewidth = 1;
  this.bezierType = 3;
}

myPolyline.bezierType = {
    accurate: 0,
    smooth: 1,
    funny: 2,
    nowidth: 3
}

myPolyline.prototype = {
    
  // getters for values
  "getPoints": function() {
    return this.points;
  },

  "getLastLine": function() {
    var indx1 = this.points.length -2;
    var indx2 = this.points.length -1;
    return [this.points[indx1].getX(),this.points[indx1].getY(),this.points[indx2].getX(),this.points[indx2].getY()];
  },

  "start": function (x,y) {
    this.addPoint(x,y);
    this.time = new Date().getTime();
  },

  "addPoint": function(x,y) {
    this.points.push(new Point(x-this.offsetX,y-this.offsetY,this.linewidth/2));
  },

  "stop": function (x,y) {
    this.addPoint(x,y);
  },

  "move": function (x,y) {
    if(this.time >= new Date().getTime()-500) {
      this.addPoint(x, y);
      this.time = new Date().getTime();
    }
  },
  "getBezier": function(bezierType) {
    if(bezierType == myPolyline.bezierType.accurate) return accurateBezier(this.points);
    if(bezierType == myPolyline.bezierType.funny) return funnyBezier(this.points);
    if(bezierType == myPolyline.bezierType.nowidth) return nowidthBezier(this.points);
    return smoothBezier(this.points);

  },

  "setLineWidth": function(linewidth) {
    this.linewidth = linewidth;
  }
};

 // Circle
function myCircle(cx,cy,r, linewidth) {
  this.cx = cx;
  this.cy = cy;
  this.r = r;
  this.linewidth = linewidth;
}

myCircle.prototype = {

  "getX" : function() { return this.cx},
  "getY" : function() { return this.cy},
  "getR" : function() { return this.r},

  "start": function (x,y) {
    this.cx = x;
    this.cy = y;
    this.r = 1;
  },

  "stop": function (x,y) {
  },

  "move": function (x,y,r) {
      this.cx = x;
      this.cy = y;
      this.r = r;
  },
  "getBezier": function(e) {
    return smoothBezier(this.points);

  },
  "setLineWidth": function(linewidth) {
    this.linewidth = linewidth;
  }
};