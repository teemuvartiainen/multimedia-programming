
/// <reference path="lib/snap.svg.js" />
/// <reference path="lib/hammer.min.js" />
/// <reference path="helpers.js" />

// timeline has an array for the frames

function Timeline(milliSeconds, svg) {

    if (milliSeconds < 1) {
        console.error("Timeline constructor: Please provide a positive length!");
        return;
    }
    if (!svg.nodeType || svg.nodeType != 1) {
        console.error("Timeline constructor: Please provide a direct reference to the SVG element as the second argument! (not a snap-wrapped one)");
        return;
    }

    // tolerance for snapping into frames, 0 for no snapping
    this.scrobbleTolerance = 0.04;

    this.frames = {};
    this.framelist = {};
	this.isPlaying = false;
	this.lastTime = Date.now();
	this.startTime = Date.now();
	this.length = milliSeconds;
	this.output = false;
	this.svg = svg;

	this.currentPosition = 0;

	this.selectedElement = false;
	this.transform = false;
	this.decorator = this.addDecorator(svg);
	this.trashcan = false;
    this.makeTrashcan(svg);

}

Timeline.prototype = {
	
    /*******************************************************************
     *
     * PUBLIC API
     *
     *******************************************************************/

    // returns true if animation is currently playing
    "playing": function() {
        return this.isPlaying;
    },
	
    // function to start the animation
    "start": function () {
        if (this.isPlaying) return this;
        this.reset();
        this.unselect();
        this.isPlaying = true;
        this.startTime = Date.now();
        this.lastTime = Date.now();
        this.loop(this);
        return this;
    },

    // stop playing the animation
    "stop": function () {
        this.isPlaying = false;
        return this;
    },

    // resets the animation
    "reset": function () {
        this.stop();
        this.update(0);
        return this;
    },
	
    // gets the index of the last frame, i.e. length-1
    "getLastFrame": function () {
        return this.length;
    },

    // initialize a single element
    // the target argument needs to be a snap-object
    "initializeSingle": function (target) {
        console.log(target);
        var el;
        if (target.type != "g") {
            el = this.transformToGroup(target);
        } else {
            el = target;
        }
        var t = el.transform().string;
        this.add(0, new Keyframe(el, "transform", t));
        return this;
    },

    // this looks for group elements in the root
    // elements that need to be initialized need to have the class name "animated-shape"
    "initializeAutomatic": function () {
        this.traverseInit(this.svg);
        return this;
    },

    // set the scrobbler to a specific time
    "scrobbleSet": function (timeMillis) {
        if (this.isPlaying || timeMillis < 0 || timeMillis > this.length) return this;

        // snap to frames

        var snapped = false;

        for (var frame in this.framelist) {
            var f = parseInt(frame, 10);
            if (Math.abs(timeMillis - f) / this.length < this.scrobbleTolerance) {
                timeMillis = f;
                snapped = true;
            }
        }

        if (this.selectedElement) {
            this.undecorate(this.selectedElement);
        }
        this.update(timeMillis, snapped);
        if (this.selectedElement) {
            this.decorate(this.selectedElement);
        }
        return this;
    },

    // set the callback function that listens to events
    "setEventListener": function (callback) {
        this.output = callback;
        return this;
    },

    // delete selected element
    "deleteCurrent": function () {
        if (this.selectedElement) {
            var e = this.selectedElement;
            this.unselect();
            this.deleteObject(e);
        }
        return this;
    },

    // delete frames at selected position
    "clearFramesAtCurrent": function () {
        this.clearFramesAt(this.currentPosition);
        return this;
    },

    // clear selection
    "unselect": function () {
        if (this.selectedElement) {
            this.undecorate(this.selectedElement);
            this.selectedElement = false;
        }
    },

    /*******************************************************************
     *
     * EVENT HANDLERS
     *
     *******************************************************************/

    "onPan": function (ev) {
        if (!this.selectedElement) return;
        this.transform.translateX = ev.deltaX;
        this.transform.translateY = ev.deltaY;

        this.requestUpdate();
    },

    "onRotate": function (ev) {
        if (!this.selectedElement) return;
        this.transform.angle = ev.rotation;
        this.requestUpdate();
    },

    "onPinch": function (ev) {
        if (!this.selectedElement) return;
        this.transform.scale = ev.scale;
        this.requestUpdate();
    },

    "onTouchEnd": function (ev) {
        if (this.selectedElement) {
            var s = this.updateElement();
            var el = Snap(this.selectedElement);
            var bb = el.getBBox();
            if (s) {
                this.add(this.currentPosition, new Keyframe(el, "transform", s));
            }
        }
    },

    "onTouchStart": function (ev) {
        if (this.selectedElement) {
            var el = Snap(this.selectedElement);
            var t = el.transform();

            var initial = TransformSplit(t.string);

            this.transform = {
                initial: initial,
                translateX: 0,
                translateY: 0,
                scale: 1,
                angle: 0,
                rx: 0,
                ry: 0,
                rz: 0
            };

        }
    },

    "onTouchMove": function (ev) {
    },

    "onTap": function (tapEvent) {
        if (this.isPlaying) return;
       
        var xx = tapEvent.center.x;
        var yy = tapEvent.center.y;

        this.clickTrashcan(xx, yy);

        this.undecorate();
        if (!this.selectAtPos(xx, yy)) {
            if (!this.selectRadius(xx, yy)) {
                this.unselect();
            }
        }
    },

    "isActive": function(eventType) {
        if(eventType == 'tap' || this.selectedElement) return true;
        return false;
    },

    /*******************************************************************
     *
     * PRIVATE API
     *
     *******************************************************************/

    // adds a new frame to the list at specified time
    "add": function (timeMillis, keyFrame) {

        if (timeMillis > this.length) {
            console.log("frame", keyFrame, "beyond animation end");
            return this;
        }

        var objectId = keyFrame.getObject().id;
        var propertyName = keyFrame.getProperty();

        if (!this.frames[objectId]) {
            this.frames[objectId] = {};
        }

        if (!this.frames[objectId][propertyName]) {
            this.frames[objectId][propertyName] = {};
        }

        this.frames[objectId][propertyName][timeMillis] = keyFrame;

        if (!this.framelist[timeMillis]) {
            this.framelist[timeMillis] = true;
            if (this.output && timeMillis > 0) {
                var e = new Event('frameadded');
                e.value = timeMillis;
                this.output(e);
            }
        }

        return this;
    },

    "select": function (element) {
        this.unselect();
        this.selectedElement = element;
        this.decorate(element);
    },

    "transformToGroup": function (snapelement) {
        // create group
        var group = snapelement.parent().group();
        group.addClass("animated-shape");

        // append group to the parent
        snapelement.parent().append(group);

        // move child into the group
        group.add(snapelement);
        var t = snapelement.transform();
        group.transform(t.local);

        // remove transform and class
        snapelement.transform("");
        snapelement.removeClass("animated-shape");
	    
        return group;
    },

    "decorate": function (element) {
        var snapelement = Snap(element);

        this.decorator.remove();
        this.trashcan.remove();

        if (snapelement.type != "g") {
            return;
        }
        var bb = snapelement.getBBox();

        var t = TransformSplit(snapelement.transform().local);
        
        this.decorator.attr(
                {
                    width: bb.w,
                    height: bb.h
                }
            );
        
        var tstr1 = "r" + (-t.angle) + " s" + (1 / t.scaleX) + " t" + (bb.x - t.translateX) + "," + (bb.y - t.translateY);
        var tstr2 = "r" + (-t.angle) + " s" + (1 / t.scaleX) + " t" + (bb.x - t.translateX + bb.w + 6) + "," + (bb.y - t.translateY);

        this.decorator.transform(tstr1);
        this.trashcan.transform(tstr2);
        
        this.trashcan.attr("visibility", "visible");
        this.decorator.attr("visibility", "visible");

        snapelement.append(this.decorator);
        snapelement.append(this.trashcan);
    },

    "undecorate": function (element) {
        this.decorator.attr("visibility", "hidden");
        this.decorator.remove();
        this.undecorateTrash(element);
    },

    "addDecorator": function (svg) {	    
        return Snap(svg).rect(0,0,10,10).attr({
            fill:     "rgba(0,0,0,0)",
            stroke: "#666",
            strokeWidth: 2,
            visibility: "hidden",
            strokeDasharray: "2,4",
            id: "selector-grid"
        });
    },

    "undecorateTrash": function (element) {
        this.trashcan.attr("visibility", "hidden");
        this.trashcan.remove();
    },

    "makeTrashcan": function(svg) {
        Snap.load("trashcan.svg", this.trashLoaded.bind(this));
    },
    
    "trashLoaded": function(payload) {
        var g = Snap(this.svg).g().attr("id","trash-can-yay");
        g.append(payload);
        Snap(this.svg).append(g);
        this.trashcan = g;
        this.trashcan.attr("visibility", "hidden");
    },

    "clickTrashcan": function(x, y){
        var target = Snap.getElementByPoint(x, y);
        if ((target.node.ownerSVGElement && target.node.ownerSVGElement.id === "trashcan")
            || target.node.id === "trash-can-yay") {
            this.deleteCurrent();
        }
    },

    "requestUpdate": function() {
        requestAnimFrame(this.updateElement.bind(this));
    },

    "updateElement": function () {
        if (this.selectedElement) {
            var el = Snap(this.selectedElement);
            var t = this.transform;
            var tstr = "t" + (t.translateX + t.initial.translateX) + "," + (t.translateY + t.initial.translateY)
	                    + "s" + (t.scale * t.initial.scaleX)
	                    + "r" + (t.angle + t.initial.angle);
            el.transform(tstr);
            return tstr;
        }
    },

    "debugPoint": function(x,y) {
        var off = $(this.svg).offset();
        Snap(this.svg).circle(x - off.left, y - off.top, 1);
    },

    // select at a point xx, yy and try to find a selection
    // with a circle around it (to counter tap inaccuracy)
    "selectRadius": function(xx, yy) {
        // tune these
        var radius = 15.0;
        var steps = 48;

        var step = (Math.PI * 2) / steps;
        for (var D = 0.0; D < Math.PI * 2; D += step) {
            var xp = xx + Math.cos(D) * radius;
            var yp = yy - Math.sin(D) * radius;

            if (this.selectAtPos(xp, yp)) {
                return true;
            }
        }
        return false;
    },

    // select exactly at xx,yy 
    // find an element in the tree with the classname that
    // means the element is selectable
    "selectAtPos": function (xx, yy) {
        target = Snap.getElementByPoint(xx, yy);
        while (!target.hasClass("animated-shape")) {
            if (target.type == "svg") {
                if (target.node.id == "trashcan") {
                    this.deleteCurrent();
                } else {
                    return false;
                }
            }
            target = target.parent();
        }
        this.select(target.node);
        return true;
    },

	"traverseInit": function (node) {
	    if (node.nodeType > 1) return;
	    var snapNode = Snap(node);
	    if (snapNode.hasClass("animated-shape")) {
	        this.initializeSingle(snapNode);
	    }
	    if (node.childNodes) {
	        var l = node.childNodes.length;
	        for (var i = 0; i < l; i++) {
	            this.traverseInit(node.childNodes[i]);
	        }
	    }
	},

	"loop": function(anim){
		
		// return if we stopped
		if (!anim.isPlaying) return;
		
		// get the time difference between start and now
		var now = Date.now();
		var timepoint = now - anim.startTime;
		
		// stop if we reached the end
		if (timepoint > this.length) {
			this.stop();
			return;
		}
		
		// interpolate states
		anim.update(timepoint);
		
		// request next frame from the browser
		anim.lastTime = now;
		requestAnimFrame(function(){anim.loop(anim);});
	},
	
	"deleteObject": function (object) {
	    this.clearFramesFor(object);
	    object.remove();
	},

	"clearFramesFor": function(snapObject) {
	    if (this.frames[snapObject.id]) {
	        delete this.frames[snapObject.id];
	    }
	},

	"clearFramesAt": function (timeMillis) {

	    if (timeMillis <= 0 || timeMillis > this.length) return;

	    for (ob in this.frames) {
	        for (prop in this.frames[ob]) {
	            for (frame in this.frames[ob][prop]) {
	                var frame_number = parseInt(frame, 10);
	                if (frame_number == timeMillis) {
	                    delete this.frames[ob][prop][frame];
	                }
	            }
	        }
	    }
	    if (this.framelist[timeMillis]) {
	        delete this.framelist[timeMillis];
	        if (this.output) {
	            var e = new Event('framedeleted');
	            e.value = timeMillis;
	            this.output(e);
	        }
	    }

	    this.update(this.currentPosition);
	},

	"update": function(currentFrame, snapped){
		
	    if (this.output) {
	        var e = new Event('scrobblechange');
	        e.value = currentFrame;
	        e.snapped = snapped ? true : false;
	        this.output(e);
	    }

	    this.currentPosition = currentFrame;

		// look for objects to be updated
		for (ob in this.frames) {
			for (prop in this.frames[ob]) {
				
				// this loop searches the closest two frames before and after the current time
				var previousFrame = 0;
				var nextFrame = this.length+1;
				
				for (frame in this.frames[ob][prop]) {
					var frame_value = parseInt(frame, 10);
					if (frame_value <= currentFrame && frame_value > previousFrame) {
						previousFrame = frame;
					}
					if (frame_value > currentFrame && frame_value < nextFrame) {
						nextFrame = frame;
					}
				}
				
				// now previousFrame is either 0 or a frame that exists
				// nextFrame is either length+1 or a frame that exists
				
				// if previous frame doesn't exists, this is an error and need to abort
				if (!this.frames[ob][prop][previousFrame]) {
					console.log("Error, no previous frame for",ob,prop,"at",currentFrame);
					this.stop();
					break;
				} 
				// if next frame doesn't exist, then we set the value as the previous frame's value
				// (this happens when we are past the last keyframe in the animation)
				else if (!this.frames[ob][prop][nextFrame]) {
					this.frames[ob][prop][previousFrame].fire();
				} 
				// otherwise we have a previous and a next frame, and we are at the middle
				// so we need to interpolate
				else {
					// t is between 0.0 and 1.0, depending how close we are to the frames
					// 0.0 is at the previous frame, 0.5 is middle, 0.9 close to the next frame...
					var t = (currentFrame-previousFrame)/(nextFrame-previousFrame);	
					
					// call the interpolation function
					var linear = this.interpolate(prop,
												  this.frames[ob][prop][previousFrame].getValue(),
												  this.frames[ob][prop][nextFrame].getValue(),
												  t);
					
					// set the value to whatever the result was
					this.frames[ob][prop][previousFrame].fireAt(linear);
				}
				
			}
		}
		
		return this;
		
	},
	
	"interpolate": function(prop, previous, next, t){
		
        // transforms can be interpolated nicely
		if (prop == "transform") {
		    // split the transforms into the different parts (translate, rotate, etc.)
		    var o1 = TransformSplit(previous);
		    var o2 = TransformSplit(next);
		    
            // now we can just do linear interpolation between them
		    var tstr = "t" + (t * o2.translateX + (1-t)* o1.translateX) + "," + (t * o2.translateY + (1-t)* o1.translateY)
	                    + "s" + (t * o2.scaleX + (1-t)* o1.scaleX)
	                    + "r" + (t * o2.angle + (1 - t) * o1.angle);

		    return tstr;
		}

	    // fallback to number interpolation if something else than transform
        // (this shouldn't happen)

		// make sure we have numbers
		var p = parseInt(previous);
		var n = parseInt(next);
		
		// basic linear interpolation
		return (t * n)+((1-t) * p);
	}

};

// Keyframe is a basic data container
function Keyframe(object, objectProperty, propertyValue) {
    this.o = object;
    this.p = objectProperty;
    this.v = propertyValue;
}

Keyframe.prototype = {

    // getters for values
    "getObject": function () {
        return this.o;
    },
    "getProperty": function () {
        return this.p;
    },
    "getValue": function () {
        return this.v;
    },

    // fire the keyframe at its value
    "fire": function () {
        this.fireAt(this.v);
    },
    
    // fire the keyframe with a modified value
    "fireAt": function (val) {
        if (this.p == "transform") {
            this.o.transform(val);
        } else {
            this.o.attr(this.p, val);
        }
    }
};