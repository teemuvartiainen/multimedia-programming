
/// <reference path="timeline.js" />
/// <reference path="lib/snap.svg.js" />
/// <reference path="lib/jquery-2.1.1.js" />
/// <reference path="lib/hammer.min.js" />

// main class for the application
function Animator(container, svg) {

    // variables
    this.mode = Animator.Mode.DRAW;
    this.svg = svg;

    // create hammer
    this.manager = false;
    this.hammer = this.createHammer(this.svg);

    // initialize timeline
    this.timeline = new Timeline(5000, this.svg);
    this.timeline.initializeAutomatic();
    this.timeline.setEventListener(this.handleTimelineEvents.bind(this));
    
    // create draw
    this.drawmode = new Drawmode($('#svgg')[0]);

    // create UI
    this.cross = false;
    this.createUI(container);

    this.currentHandler = this.drawmode;

    //for allowing the user to zoom and move the canvas
    this.canvas = Snap('#svgg');
    this.canvasTransform = this.canvas.transform().localMatrix;
    this.originalCanvasTransform = this.canvasTransform;
    this.moveCanvas = false;
}

Animator.Mode = {
    DRAW: 0,
    ANIMATE: 1
}

// class methods
Animator.prototype = {

    // UI callbacks
    // from control buttons

    uiChangeMode: function(mode) {
        this.mode = mode;
        if (mode === Animator.Mode.ANIMATE) {
            //hide grid
            $('#backgroundGrid').hide();

            // change button colors 
            $("#draw").prop('disabled', false).removeClass("mode-selected");
            $("#animate").prop('disabled', true).addClass("mode-selected");

            // enable edit buttons
            $("#delete").prop('disabled', false); $("#remove").prop('disabled', false);
            this.currentHandler = this.timeline;
            this.canvas.transform(this.originalCanvasTransform);

        } else if (mode === Animator.Mode.DRAW) {
            //show grid
            $('#backgroundGrid').show();

            // change button colors
            $("#draw").prop('disabled', true).addClass("mode-selected");
            $("#animate").prop('disabled', false).removeClass("mode-selected");

            // disable edit buttons
            $("#delete").prop('disabled', true); $("#remove").prop('disabled', true);
            this.timeline.unselect();
            this.currentHandler = this.drawmode;
            this.resetCanvas();

        }
    },

    uiPlay: function() {
        this.timeline.start();
        this.resetCanvas();
    },

    uiStop: function () {
        this.timeline.reset();
        this.resetCanvas();
    },

    uiClearFrame: function () {
        this.timeline.clearFramesAtCurrent();
    },

    uiDeleteObject: function () {
        this.timeline.deleteCurrent();
    },

    uiScrobble: function(value) {
        this.timeline.scrobbleSet(value)
    },

    // timeline callback

    handleTimelineEvents: function(event) {
        if (event.type == "scrobblechange") {

            // handle cross things
            if (event.snapped && event.value > 0) {
                var perc = Math.floor(event.value / this.timeline.getLastFrame() * 100000) / 1000;
                var id = "#marker-" + perc;
                id = id.replace(".", "-");
                var o = $(id).offset();
                this.cross.css({
                    top: (o.top - 50) + "px",
                    left: (o.left - 13) + "px"
                }).show();
            } else {
                this.cross.hide();
            }

            // move scrobbler to the frame given
            $("#scrobble").val(event.value);
            return false;

        } else if (event.type == "frameadded") {

            // calculate new frame position
            var perc = Math.floor(event.value / this.timeline.getLastFrame() * 100000) / 1000;
            var id = "marker-" + perc;
            id = id.replace(".", "-");

            // add new marker to the position given
            var marker = $("<div>").attr("id", id).addClass("marker").css("left", perc + "%");
            $("#scrobblewrap").append(marker);

        } else if (event.type == "framedeleted") {

            // calculate frame position
            var perc = Math.floor(event.value / this.timeline.getLastFrame() * 100000) / 1000;
            var id = "#marker-" + perc;
            id = id.replace(".", "-");

            // delete marker
            $(id).remove();

        } else {
            console.log("Unknown event", event.type, event);
        }
    },

    // clear canvas transformation
    resetCanvas: function() {
        this.canvas.transform(this.originalCanvasTransform);
        this.drawmode.setLineWidthScale(1);
    },

    // initialization functions

    createHammer: function(target){
        var mc = new Hammer.Manager(target);

        mc.add(new Hammer.Pan({ threshold: 5, pointers: 0 }));
        mc.add(new Hammer.Rotate({ threshold: 0 })).recognizeWith(mc.get('pan'));
        mc.add(new Hammer.Pinch({ threshold: 0 })).recognizeWith([mc.get('pan'), mc.get('rotate')]);
        mc.add(new Hammer.Tap({ time: 800 }));

        mc.on("panstart panmove", this.onPan.bind(this));
        mc.on("rotatestart rotatemove", this.onRotate.bind(this));
        mc.on("pinchstart pinchmove", this.onPinch.bind(this));
        mc.on("tap", this.onTap.bind(this));

        mc.on("hammer.input", function (ev) {
            if (ev.isFinal) {
                this.onTouchEnd(ev);
            }
            if (ev.isFirst) {
                this.onTouchStart(ev);
            }
        }.bind(this));

        this.manager = mc;
    },

    createUI: function (container) {
        var cwSnap = Snap(50,50);
        cwSnap.transform(new Snap.Matrix()).attr({id:'cwsvg'});

        var lwSnap = Snap(150, 150).attr({ id: 'lwsvg' });

        this.cross = $("<div>").addClass("framedeleter").click(
                    function (ev) {
                        this.timeline.clearFramesAtCurrent();
                        ev.preventDefault();
                        return false;
                    }.bind(this)
                );
        this.cross.load('cross.svg');

        $(container).append(
            $("<div>").attr({
                id: "toolbar"
            }
            ).append(
                $("<input>").attr({ type: "button", id: "help" }).val("Help")
                    .click(function () {
                        if($(this).hasClass('mode-selected')) {
                            $('#toolbar label').hide();
                            $(this).removeClass('mode-selected');
                        } else {
                            $(this).addClass('mode-selected');
                            $('#toolbar label').show();
                        }
                })
            ).append(
                $("<input>").attr({ type: "button", id: "draw" }).val("Draw").addClass("mode-selected").prop('disabled', true)
                    .click(function () {
                        animator.uiChangeMode(Animator.Mode.DRAW);
                    })
            ).append(
                $("<label>Select this to draw new objects.</label>")
            ).append(
                $("<input>").attr({ type: "button", id: "animate" }).val("Anim")
                    .click(function () {
                        animator.uiChangeMode(Animator.Mode.ANIMATE);
                    })
            ).append(
                $("<label>Select this to animate or delete objects.</label>")
            ).append(
                cwSnap.node
            ).append(
                $("<label>Select color.</label>")
            ).append(
                lwSnap.node
            ).append(
                $("<label>Select linewidth.</label>")
            ).append(
                $("<input>").attr({ type: "button", id: "drawtool" }).val("Curve")
                    .click(function () {
                        $(this).val(animator.drawmode.changeTool());
                        if($(this).val() == 'Curve') {
                            $('#toolmode').show();
                        } else {
                            $('#toolmode').hide();
                        }
                    })
            ).append(
                $("<label>Select drawing tool (curve or circle)</label>")
            ).append(
                $("<input>").attr({ type: "button", id: "entergroup" }).val("Group")
                    .click(function () {
                        if(!animator.drawmode.inGroup) { 
                            animator.drawmode.enterGroup()
                            $(this).addClass('mode-selected');
                        } else {
                            animator.drawmode.exitGroup()
                            $(this).removeClass('mode-selected');
                        }
                    })
            ).append(
                $("<label>Draw in group, ie. everything drawn becomes part of a single animatable object.</label>")
            ).append(
                $("<input>").attr({ type: "button", class: "toolmode",id: "toolmode" }).val("smooth")
                    .click(function () {
                        $(this).val(animator.drawmode.toggleToolMode());
                    })
            ).append(
                $("<label class=\"toolmode\">Select Bezier interpolation mode.</label>")
            )
        ).append(
            $("<div>").attr("id", "scrobblewrap").append(
                $("<input>").attr({
                    id: "scrobble",
                    type: "range",
                    min: "0",
                    max: this.timeline.getLastFrame()
                }).val(0).change(function () {
                    animator.uiScrobble($(this).val());
                })
            )
        ).append(
            $("<input>").attr({ type: "button", id: "play" }).val("Play")
                    .click(function () {
                        animator.uiPlay();
                    })
        ).append(
            $("<input>").attr({ type: "button", id: "reset" }).val("Stop")
                    .click(function () {
                        animator.uiStop();
                    })
        ).append(
            this.cross
        );
        $('#toolbar label').hide();
        this.colorwheel = new Colorwheel(cwSnap, 400, 60);
        this.linewidth = new LineWidth(lwSnap,5,60,1,this.drawmode.setLineWidth.bind(this.drawmode));
    },


    // Handlers
    onPan: function (event) {
        if(this.currentHandler.isActive('pan') && !this.moveCanvas) {
            var newEvent = new Object();
            newEvent.x = this.canvasTransform.invert().x(event.changedPointers[0].pageX, event.changedPointers[0].pageY);
            newEvent.y = this.canvasTransform.invert().y(event.changedPointers[0].pageX, event.changedPointers[0].pageY);
            newEvent.deltaX = event.deltaX;
            newEvent.deltaY = event.deltaY;
            newEvent.center = new Object();
            newEvent.center.x = this.canvasTransform.invert().x(event.center.x,event.center.y);
            newEvent.center.y = this.canvasTransform.invert().y(event.center.x,event.center.y);
            this.currentHandler.onPan(newEvent);
        } else {
            if (animator.mode === Animator.Mode.DRAW) {
                this.moveCanvas = true;
                if(!this.canvasTransform) {
                    this.canvasTransform = this.canvas.transform().localMatrix;
                }
                this.canvas.transform(new Snap.Matrix().scale(event.scale).translate(event.deltaX, event.deltaY).add(this.canvasTransform));
            }
        }

        event.preventDefault();
        return false;
    },

    onPinch: function (event) {
        if(this.currentHandler.isActive('pinch') && !this.moveCanvas) {
            this.currentHandler.onPinch(event);
        } else {
            if (animator.mode === Animator.Mode.DRAW) {
                this.moveCanvas = true;
                if(!this.canvasTransform) {
                    this.canvasTransform = this.canvas.transform().localMatrix;
                }
                this.canvas.transform(new Snap.Matrix().scale(event.scale).translate(event.deltaX, event.deltaY).add(this.canvasTransform));
            }
        }        
        event.preventDefault();
        return false;
    },

    onRotate: function (event) {
        if(this.currentHandler.isActive('rotate') && !this.moveCanvas) {
            this.currentHandler.onRotate(event);
        } else {
            // let's not rotate canvas
        }   
        event.preventDefault();
        return false;
    },

    onTap: function (event) {
        if(this.currentHandler.isActive('tap') && !this.moveCanvas) {
            this.currentHandler.onTap(event);
        }
        event.preventDefault();
        return false;
    },

    onTouchStart: function (event) {
        if(this.mode === Animator.Mode.DRAW) {
            this.drawmode.setColor(this.colorwheel.getSelectedColor().toString());
        }
        this.currentHandler.onTouchStart(event);
        
        event.preventDefault();
        return false;
    },

    onTouchEnd: function (event) {
        if(this.currentHandler.isActive('touchend') && !this.moveCanvas) {
            event.center.x = this.canvasTransform.invert().x(event.center.x, event.center.y);
            event.center.y = this.canvasTransform.invert().y(event.center.x, event.center.y);
            // in drawmode this returns the svg-element that was drawn
            var result = this.currentHandler.onTouchEnd(event);

            if(this.mode === Animator.Mode.DRAW) {
                this.timeline.initializeSingle(animator.drawmode.getLastDoodle());
            }
        } else {
            if (animator.mode === Animator.Mode.DRAW) {
                this.moveCanvas = false;
                this.canvasTransform = this.canvas.transform().localMatrix;
                this.drawmode.setLineWidthScale(1/this.canvasTransform.a);
            }
        }
        event.preventDefault();
        return false;
    },

    onTouchMove: function (event) {
        this.currentHandler.onTouchMove(event);
        event.preventDefault();
        return false;
    }

};