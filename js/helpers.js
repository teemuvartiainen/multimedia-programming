// This is some cross browser compatible proofing
// requestAnimFrame requests a next window "paint" event
// this enables a nice frame rate that runs exactly as fast as the browser allows
window.requestAnimFrame = (function () {
    return window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            function (callback) {
                window.setTimeout(callback, 1000 / 60);
            };
})();


// read individual values from a snap transform string
var translateMatch = new RegExp("t([0-9\-\.]+)\,([0-9\-\.]+)", "i");
var rotateMatch = new RegExp("r([0-9\-\.]+)", "i");
var scaleMatch = new RegExp("s([0-9\-\.]+)", "i");
function TransformSplit(str) {
    var ob = {
        translateX: 0,
        translateY: 0,
        scaleX: 1,
        angle: 0
    };
    var tr = str.match(translateMatch);
    if (tr) {
        ob.translateX = parseFloat(tr[1]);
        ob.translateY = parseFloat(tr[2]);
    }
    var rt = str.match(rotateMatch);
    if (rt) {
        ob.angle = parseFloat(rt[1]);
    }
    var sc = str.match(scaleMatch);
    if (sc) {
        ob.scaleX = parseFloat(sc[1]);
    }
    return ob;
}