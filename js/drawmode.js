/* drawmode.js takes care of:
 - passing events on to the right drawing code
 - color selection
 - linewidth selection

 Currently supported drawing tools are:
 - polyline/bezier

Assumes:
- we draw in #svg

Needs: 
- bezier.js
- snap.svg.js
- jquery

 */

function Drawmode(svg) {
	this.snap = Snap(svg);
	this.drawing = false;
	this.drawtarget = this.snap;
	this.drawtargetParent = false;
  this.drawcolor = "#000";
  this.linewidth = 5;
  this.linewidthscale = 1;

	//drawing tools
	this.selectedTool = Drawmode.Drawtool.polyline;
	this.doodle = false; //object that is being drawn
	this.tmpgroup = false;  //svg group in which we draw freehand polyline, before calculating bezier
  this.beziermode = myPolyline.bezierType.smooth;

  // allow drawing in group
  this.inGroup = false;
  this.lastDoodle = false;
}

Drawmode.Drawtool = {
	polyline: 0,
  circle: 1
}

Drawmode.prototype = {

  "changeTool": function() {
    if(this.selectedTool == Drawmode.Drawtool.polyline) {
      this.selectedTool = Drawmode.Drawtool.circle;
      return 'Circle';
    } else if (this.selectedTool == Drawmode.Drawtool.circle) {
      this.selectedTool = Drawmode.Drawtool.polyline;
      return 'Curve';
    }
  },

  "toggleToolMode": function() {
    if(this.selectedTool == Drawmode.Drawtool.polyline) {
      this.beziermode = (this.beziermode+1)%4;
      if(this.beziermode == 0) return 'accu.';
      if(this.beziermode == 1) return 'smooth';
      if(this.beziermode == 2) return 'funny';
      if(this.beziermode == 3) return 'no-width';
    }
  },

  "getColor" : function() {
    return this.drawcolor;
  },

  "setColor" : function(color) {
    this.drawcolor = color;
  },

  "getLastDoodle" : function() {
    if(this.inGroup) {
      return this.drawtarget;  
    } else {
      return this.lastDoodle;
    }
    
  },

  "enterGroup": function(ev) {
    if(this.inGroup) return 'already in group';
    this.drawtargetParent = this.drawtarget;
    this.drawtarget = this.drawtarget.g();
    this.drawtarget.addClass("animated-shape");
    this.drawtarget.attr({ stroke: this.getColor() });
    this.inGroup = true;
    return 'entered group';
  },

  "exitGroup": function(ev) {
    if(!this.inGroup) return 'not in group';
    this.inGroup = false;
  	this.drawtarget = this.drawtargetParent;
  	if(this.drawtarget == false || this.drawtarget == this.snap) {
  		this.drawtarget = this.snap;
  		this.drawtargetParent = false;
  	}
  	return 'exited group';
  },

  "setLineWidth": function(linewidth) {
    if(this.doodle) {
      this.doodle.setLineWidth(linewidth*this.linewidthscale);
    }
    this.linewidth = linewidth;
  },

  "setLineWidthScale": function(lws) {
    this.linewidthscale = lws;
    if(this.doodle) {
      this.doodle.setLineWidth(this.linewidth*this.linewidthscale)
    }
  },

  "start": function(ev) {
    this.tmpgroup = this.snap.g();
    this.tmpgroup.attr({ stroke: this.getColor(), fill: this.getColor() });
    if(this.selectedTool == Drawmode.Drawtool.polyline) {
      //drawing polyline
      var svgpos = $(this.snap.node).offset();
      this.doodle = new myPolyline(0,0, this.tmpgroup);
    } else if (this.selectedTool == Drawmode.Drawtool.circle) {
      this.doodle = new myCircle(ev.center.x, ev.center.y,1,1);
    }   
    this.doodle.start(ev.center.x, ev.center.y);
    this.doodle.setLineWidth(this.linewidth*this.linewidthscale);
    this.drawing = true;
  },

  /* start drawing */
  "onTouchStart": function (ev) {
  },

  /* finish drawing */
  "onTouchEnd": function (ev) {
    if(!this.drawing) return;


    this.drawing = false;

    var doodlesvg;
    if(this.selectedTool == Drawmode.Drawtool.polyline) {
      doodlesvg = this.snap.path(this.doodle.getBezier(this.beziermode));
    } else if (this.selectedTool == Drawmode.Drawtool.circle) {
      doodlesvg = this.snap.circle(this.doodle.getX(), this.doodle.getY(), this.doodle.getR());
    }
    doodlesvg.attr({
     stroke: this.getColor(),
     strokeWidth: 1,
     fill: this.getColor()
    });

    if(!this.inGroup) {
      doodlesvg.attr({class: "animated-shape"});
    }

    if(this.beziermode == myPolyline.bezierType.funny && this.selectedTool == Drawmode.Drawtool.polyline) {
      doodlesvg.attr({fill: 'none'});
    }
    if(this.beziermode == myPolyline.bezierType.nowidth && this.selectedTool == Drawmode.Drawtool.polyline) {
     doodlesvg.attr({fill: 'none', strokeWidth: this.linewidth}); 
    }

    this.drawtarget.add(doodlesvg);

    this.lastDoodle = doodlesvg;

    //clear tmpgroup
    this.tmpgroup.remove();

    return doodlesvg; 
  },

  "onPan": function(ev) {
    if (!this.drawing) {
      this.start(ev);
    }
    var x = ev.x;
    var y = ev.y;
    if(this.selectedTool == Drawmode.Drawtool.polyline) {
      this.doodle.move(x, y);
      //drawing polyline
      this.tmpgroup.add(this.snap.polyline(this.doodle.getLastLine()).attr({strokeWidth: this.linewidth}));
    } else if(this.selectedTool == Drawmode.Drawtool.circle) {
      var r = Math.sqrt((ev.deltaX*ev.deltaX)+(ev.deltaY*ev.deltaY))/2;
      this.doodle.move(this.doodle.getX(), this.doodle.getY(), r);
      this.tmpgroup.clear();
      this.tmpgroup.add(this.snap.circle(this.doodle.getX(),this.doodle.getY(),r));
    }
  },

  "onPinch": function (ev) {
  },

  "onTap": function(ev) {
  },

  "onRotate": function(ev) {
  },

  "onTouchMove": function(ev) {
  },

  "requestUpdate": function() {
      requestAnimFrame(this.updateElement.bind(this));
  },

  "updateElement": function () {

  },

  "isActive": function(eventType) {
    if(eventType === 'pan') {
      return true;
    }
    return this.drawing;
  },


}
