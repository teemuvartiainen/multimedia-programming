
/// <reference path="lib/snap.svg.js" />
/// <reference path="lib/hammer.min.js" />

/* what do we need to do?

draw a colorwheel, with N segments
- segment size = 360° / N
- segment k, color in hsv, so s= k/N

each segment will be drawn as a colored segment 
with black->color->white gradient

The center of the colorwheel, will be bw-gradient circle
which will be drawn last on top of everything else
*/


function LineWidth(snap,start,upper_limit,lower_limit, onUpdate){
  this.mysnap = snap;
  this.upperLimit = upper_limit/start;
  this.lowerLimit = lower_limit/start;
  this.linewidth = start; 
  this.scale = 1;
  this.lw = undefined;
  this.size = start;
  this.isOpen = true;

  this.create();
  this.transform = 's1';
  this.onUpdate = onUpdate;

}

LineWidth.prototype = {

  "create_circle": function(mygroup) {
    this.lw = this.mysnap.circle(0,0,this.size).attr({
        fill:'#000000', id:'LineWidth'
        })
    mygroup.add(this.lw);    
  },

  "create": function() {
    this.topgroup = this.mysnap.g().attr({id:'LineWidth-parent', transform:'t50,100'});
    this.topgroup.add(this.mysnap.rect(-50,-100,150,150,80,10)).attr({fill:'#ddd'});
    this.create_circle(this.topgroup);

    this.topgroup.add(this.mysnap.path("M 28,50C 28,48.8954 28.8954,48 30,48L 32,48L 26.5,38L 34.25,19L 41.75,19L 49.5,38L 44,48L 46,48C 47.1045,48 48,48.8954 48,50L 48,53.5C 48,55.1568 46.1568,57 44.5,57L 31.5,57C 29.8432,57 28,55.1568 28,53.5L 28,50 Z M 38,39C 36.3431,39 35,37.6569 35,36C 35,34.6938 35.8348,33.5826 37,33.1707L 37,22L 36.3947,22L 30.25,38L 35.75,48L 40.25,48L 45.75,38L 39.6052,22L 39,22L 39,33.1707C 40.1652,33.5826 41,34.6938 41,36C 41,37.6569 39.6568,39 38,39 Z M 31,51L 31,52.5C 31,53.3284 31.6716,54 32.5,54L 43.5,54C 44.3284,54 45,53.3284 45,52.5L 45,51L 31,51 Z ").attr({
      fill:"#000000", strokeWidth:"0.2", strokeLinejoin:"round", transform:'t15,-90r-135s2'}));
    this.setupGlobalEventHandlers();
    this.close();
  },

  "close": function() {
    this.isOpen = false;
    this.topgroup.attr({transform: new Snap.Matrix().scale(0.7).translate(-20,92)});
    $(this.mysnap.node).css({height: 50, width: 50}); 
  },

  "open": function() {
    this.isOpen = true;
    this.topgroup.attr({transform: new Snap.Matrix().scale(1).translate(50,100)});
    $(this.mysnap.node).css({height: 150, width: 150}); 
  },

  "onTap": function(ev) {
    if(event instanceof MouseEvent) return false;
    if(this.isOpen) {
      this.close();
    }
    else {
      this.open();
    }
  },

  "onPinch": function (ev) {
    if(event instanceof MouseEvent) return false;
    if(this.isOpen == false) return;
      this.transform.scale = ev.scale;

      var sumscale = this.transform.scale * this.transform.initial.scaleX;

      if(sumscale > this.upperLimit) {
        this.transform.scale = this.upperLimit/this.transform.initial.scaleX;
      }
      else if(sumscale < this.lowerLimit) {
        this.transform.scale = this.lowerLimit/this.transform.initial.scaleX;
      }

      this.requestUpdate();

  },

  "onTouchEnd": function (ev) {
    if(event instanceof MouseEvent) return false;
    if(this.isOpen == false) return;
    this.updateElement();
  },

  "onTouchStart": function (ev) {
    if(event instanceof MouseEvent) return false;
    if(this.isOpen == false) return;
    var t = this.lw.transform();
    var initial = TransformSplit(t.string);

    this.transform = {
      initial: initial,
      translateX: 0,
      translateY: 0,
      scale: 1,
      angle: 0,
      rx: 0,
      ry: 0,
      rz: 0
    };
  },

  "requestUpdate": function() {
      requestAnimFrame(this.updateElement.bind(this));
  },

  "updateElement": function () {
    var t = this.transform;

    var tstr = "t" + t.initial.translateX + "," + t.initial.translateY
      + "s" + (t.scale * t.initial.scaleX)
      + "r" + t.initial.angle;
    this.lw.transform(tstr);
    this.scale = (t.scale * t.initial.scaleX);
    this.onUpdate(this.getLineWidth());
    return tstr;
  },
  "setupGlobalEventHandlers": function () {
    var mc = new Hammer.Manager(this.mysnap.node);

    mc.add(new Hammer.Pinch({ threshold: 0 }));
    mc.add(new Hammer.Tap({threshold: 5, time: 1000}));      

    mc.on("pinchstart pinchmove", this.onPinch.bind(this));
    mc.on("tap",this.onTap.bind(this));

    mc.on("hammer.input", function (ev) {
      if(ev.isFirst) {
        this.onTouchStart(ev);
      } else if(ev.isFinal) {
        this.onTouchEnd(ev);
      }
    }.bind(this));

    this.manager = mc;
  },

  "getLineWidth": function() {
    return this.linewidth * this.scale;
  }
};