/*
Point object definition.
Simple object with two coordinates, and the necessary getters.
Also contains an element representing the state of zoom when the point was drawn.
*/
function Point (x, y, width)
{
	this.x = x;
	this.y = y;
	this.width = (width ? width : 0.5);
}

Point.prototype =
{
	// Getters
	"getX": function()
	{
		return this.x;
	},
	"getY": function()
	{
		return this.y;
	},
	"getWidth": function()
	{
		return this.width;
	},
};


/*
Creates a smooth Bézier curve. Some details are lost due to the approximation needed to obtain a smooth curve.
Returns a string containing the definition of the Bézier curve.
*/
function smoothBezier (polyline, angleToleranceCur, angleTolerancePrev)
{
	angleToleranceCur = (angleToleranceCur ? angleToleranceCur : 1.75);
	angleTolerancePrev = (angleTolerancePrev ? angleTolerancePrev : 0.25);
	
	return bezierModeOne(lineWidth(lineReducer(polyline, angleToleranceCur, angleTolerancePrev)));
}


/*
Creates a smooth Bézier curve. Some details are lost due to the approximation needed to obtain a smooth curve.
Returns a string containing the definition of the Bézier curve.
*/
function nowidthBezier (polyline, angleToleranceCur, angleTolerancePrev)
{
	angleToleranceCur = (angleToleranceCur ? angleToleranceCur : 1.75);
	angleTolerancePrev = (angleTolerancePrev ? angleTolerancePrev : 0.25);
	
	return bezierModeOne(lineReducer(polyline, angleToleranceCur, angleTolerancePrev));
}

/*
Creates a accurate Bézier curve. All the details are here, but on the other hand the curve doesn't look smooth at all.
Returns a string containing the definition of the Bézier curve.
*/
function accurateBezier (polyline)
{
	return bezierModeOne(lineWidth(polyline));
}

/*
Creates a WTF Bézier curve. It looks like a curve made of several stripes.
Returns a string containing the definition of the Bézier curve.
*/
function funnyBezier (polyline)
{
	return bezierModeTwo(lineWidth(polyline));
}


/*
Creates a Bézier curve by an application of the first algorithm, on the given polyline.
The given polyline must contain at least three points and be transmitted in the form of an array of Point objects.
Returns a string containing the definition of the Bézier curve.
*/
function bezierModeOne (polyline)
{
	if (polyline.length < 3)
	{
		return "YOU ARE KIDDING ME!";
	}
	
	// Calculation of the first point, and initalization of the array
	var T0x = (polyline[1].getX() - polyline[0].getX()) / 2;
	var T0y = (polyline[1].getY() - polyline[0].getY()) / 2;
	var T0 = new Point (T0x, T0y);
	var T = [T0];
	
	// Main loop for the calculation of the points from 1 to n - 2
	for (var i = 1; i < polyline.length - 1; i++)
	{
		var Tix = (polyline[i + 1].getX() - polyline[i - 1].getX()) / 2;
		var Tiy = (polyline[i + 1].getY() - polyline[i - 1].getY()) / 2;
		var Ti = new Point (Tix, Tiy);
		T.push(Ti);
	}
	
	// Last point
	var TnMinus1x = (polyline[polyline.length - 1].getX() - polyline[polyline.length - 2].getX()) / 2;
	var TnMinus1y = (polyline[polyline.length - 1].getY() - polyline[polyline.length - 2].getY()) / 2;
	var TnMinus1 = new Point (TnMinus1x, TnMinus1y);
	T.push(TnMinus1);
	
	
	// Calculation of the control points, and construction of the Bézier curve
	var B1 = [];
	var B2 = [];
	
	for (var i = 0; i < T.length - 1; i++)
	{
		var B1ix = polyline[i].getX() + T[i].getX() / 3;
		var B1iy = polyline[i].getY() + T[i].getY() / 3;
		var B1i = new Point (B1ix, B1iy);
		B1.push(B1i);
		
		var B2ix = polyline[i + 1].getX() - T[i + 1].getX() / 3;
		var B2iy = polyline[i + 1].getY() - T[i + 1].getY() / 3;
		var B2i = new Point (B2ix, B2iy);
		B2.push(B2i);
	}
	
	// Construction and returning of the final Bézier curve
	var curve = "M " + polyline[0].getX() + "," + polyline[0].getY() + " ";
	
	// B1 and B2 have the same length
	for (var i = 0; i < B1.length; i++)
	{
		curve += ("C " + B1[i].getX() + "," + B1[i].getY() + " ");
		curve += (B2[i].getX() + "," + B2[i].getY() + " ");
		curve += (polyline[i + 1].getX() + "," + polyline[i + 1].getY() + " ");
	}
	
	return curve;
}


/*
Creates a Bézier curve by applying a different algorithm.
The given polyline must contain at least three points and be transmitted in the form of an array of Point objects.
Returns a string containing the definition of the Bézier curve.
*/
function bezierModeTwo (polyline)
{
	if (polyline.length < 3)
	{
		return "YOU ARE KIDDING ME!";
	}
	
	// Distance of the control points alongside the tangent vector
	var distance = 200;
	
	var B1 = [];
	var B2 = [];
	
	// Main loop for all points except first and last
	for (var i = 1; i < polyline.length - 1; i++)
	{
		// Calculation of the normed tangent vector for the given point
		var uxTemp = polyline[i + 1].getX() - polyline[i - 1].getX();
		var uyTemp = polyline[i + 1].getY() - polyline[i - 1].getY();
		var norma = Math.sqrt(Math.pow(uxTemp, 2) + Math.pow(uyTemp, 2));
		
		var ux, uy;
		if (norma != 0)
		{
			ux = uxTemp / norma;
			uy = uyTemp / norma;
		}
		else
		{
			ux = 0;
			uy = 0;
		}
		
		// Calculation of the control point positions
		var B1ix = polyline[i].getX() - distance * ux;
		var B1iy = polyline[i].getY() - distance * uy;
		var B1i = new Point(B1ix, B1iy);
		B1.push(B1i);
		
		var B2ix = polyline[i].getX() + distance * ux;
		var B2iy = polyline[i].getY() + distance * uy;
		var B2i = new Point(B2ix, B2iy);
		B2.push(B2i);
	}
	
	// Construction and returning of the final Bézier curve
	var curve = "M " + polyline[0].getX() + "," + polyline[0].getY() + " ";
	
	// B1 and B2 have the same length
	for (var i = 0; i < B1.length; i++)
	{
		curve += ("C " + B2[i].getX() + "," + B2[i].getY() + " ");
		curve += (B1[i].getX() + "," + B1[i].getY() + " ");
		curve += (polyline[i + 1].getX() + "," + polyline[i + 1].getY() + " ");
	}
	
	return curve;
}


/*
Takes a polyline as input, and then reduces the number of points where a simple line was actually drawn.
The given polyline must be transmitted in the form of an array of Point objects.
Two angles must also be transmitted as arguments: the angle tolerance from the current point (last point added to the output curve),
	and the angle tolerance from the previous point of the input polyline.
Returns another polyline with far less points, to reduce the complexity of the following calculations.
*/
function lineReducer (polyline, angleToleranceCur, angleTolerancePrev)
{
	// Conversion of the angles
	var angleTol1 = angleToleranceCur * Math.PI / 180;
	var angleTol2 = angleTolerancePrev * Math.PI / 180;
	
	// Initialization of the current point and the output polyline
	var currentPoint = 0;
	var P = [polyline[0]];
	
	// Loop on the given polyline, in order to only keep and return the interesting points
	for (var i = 1; i < polyline.length; i++)
	{
		// Calculation of the angle between the current point and the next point
		var angle11 = Math.atan2(polyline[currentPoint].getY(), polyline[currentPoint].getX());
		var angle12 = Math.atan2(polyline[i].getY(), polyline[i].getX());
		var angle1 = Math.abs(angle11 - angle12);
		
		// Calculation of the angle between the last point and the next point
		var angle21;
		if (i > 1)
		{
			angle21 = Math.atan2(polyline[i - 2].getY(), polyline[i - 2].getX());
		}
		else
		{
			angle21 = Math.atan2(polyline[i - 1].getY(), polyline[i - 1].getX());			
		}
		var angle22 = Math.atan2(polyline[i].getY(), polyline[i].getX());
		var angle2 = Math.abs(angle21 - angle22);
		
		// If we detect an angle greater than the tolerance, we save the point and take it as the next current point
		if (angle1 > angleTol1 && angle2 > angleTol2)
		{
			if (i > 1)
			{
				P.push(polyline[i - 1]);
			}
			else
			{
				P.push(polyline[i]);
			}
			currentPoint = i;
		}
	}
	
	// We always need to have the last point in the output, in order not to lose a part of the polyline
	P.push(polyline[polyline.length - 1]);
	
	return P;
}


/*
Takes a polyline as input, and then transforms it into the shape of the curve to be drawn.
The given polyline must be transmitted in the form of an array of Point objects.
Returns another polyline with the skeleton of the curve, to be transmitted to the Bézier transformation function.
*/
function lineWidth (polyline)
{
	var T = new Array (polyline.length * 2);
	
	// Main loop for all points except the last
	for (var i = 0; i < polyline.length - 1; i++)
	{
		// Calculation of the angle between the abscisse line and the next segment, and deducing unit vector from that
		var alpha = Math.atan2(polyline[i + 1].getY() - polyline[i].getY(), polyline[i + 1].getX() - polyline[i].getX());
		var ux = - Math.sin(alpha);
		var uy = Math.cos(alpha);
		
		// Creation of the two substitute points for the skeleton (nobody cares about the width value anymore, so let's put π)
		Ti1x = polyline[i].getX() + polyline[i].getWidth() * ux;
		Ti1y = polyline[i].getY() + polyline[i].getWidth() * uy;
		Ti2x = polyline[i].getX() - polyline[i].getWidth() * ux;
		Ti2y = polyline[i].getY() - polyline[i].getWidth() * uy;
		Ti1 = new Point (Ti1x, Ti1y, Math.PI);
		Ti2 = new Point (Ti2x, Ti2y, Math.PI);
		
		// Storing of these points in the right location in the array: one is on the “first way” and the other on the “return” when scanning the polyline
		T[i] = Ti1;
		T[2 * polyline.length - i - 1] = Ti2;
	}
	
	// Last point: taking the previous segment rather than the next one, because there is no next one!
	var alpha = Math.atan2(polyline[polyline.length - 1].getY() - polyline[polyline.length - 2].getY(), polyline[polyline.length - 1].getX() - polyline[polyline.length - 2].getX());
	var ux = - Math.sin(alpha);
	var uy = Math.cos(alpha);
	
	// Creation of the two substitute points for the skeleton (nobody cares about the width value anymore, so let's put π)
	Tn1x = polyline[polyline.length - 1].getX() + polyline[polyline.length - 1].getWidth() * ux;
	Tn1y = polyline[polyline.length - 1].getY() + polyline[polyline.length - 1].getWidth() * uy;
	Tn2x = polyline[polyline.length - 1].getX() - polyline[polyline.length - 1].getWidth() * ux;
	Tn2y = polyline[polyline.length - 1].getY() - polyline[polyline.length - 1].getWidth() * uy;
	Tn1 = new Point (Tn1x, Tn1y, Math.PI);
	Tn2 = new Point (Tn2x, Tn2y, Math.PI);

	// Storing of these points in the right location in the array: one is on the “first way” and the other on the “return” when scanning the polyline
	T[polyline.length - 1] = Tn1;
	T[polyline.length] = Tn2;
	
	
	return T;
}