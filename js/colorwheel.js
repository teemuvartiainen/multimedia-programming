
/// <reference path="lib/snap.svg.js" />
/// <reference path="lib/hammer.min.js" />

/* what do we need to do?

draw a colorwheel, with N segments
- segment size = 360° / N
- segment k, color in hsv, so s= k/N

each segment will be drawn as a colored segment 
with black->color->white gradient

The center of the colorwheel, will be bw-gradient circle
which will be drawn last on top of everything else
*/


// read individual values from a snap transform string
var translateMatch = new RegExp("t([0-9\-\.]+)\,([0-9\-\.]+)", "i");
var rotateMatch = new RegExp("r([0-9\-\.]+)", "i");
var scaleMatch = new RegExp("s([0-9\-\.]+)", "i");
function TransformSplit(str) {
    var ob = {
        translateX: 0,
        translateY: 0,
        scaleX: 1,
        angle: 0
    };
    var tr = str.match(translateMatch);
    if (tr) {
        ob.translateX = parseFloat(tr[1]);
        ob.translateY = parseFloat(tr[2]);
    }
    var rt = str.match(rotateMatch);
    if (rt) {
        ob.angle = parseFloat(rt[1]);
    }
    var sc = str.match(scaleMatch);
    if (sc) {
        ob.scaleX = parseFloat(sc[1]);
    }
    return ob;
}

function Colorwheel(snap, color_wheel_size, no_segments){
  this.cwsize = color_wheel_size || 1;
  this.mysnap = snap;
  this.segments = no_segments || 60;
  this.limit = this.cwsize/4;
  this.cwmatrix = false;
  this.cw = false;
  this.selectedHSL = false; 
  this.isOpen = true;
  this.create(color_wheel_size);

  this.transform = false;
  this.parentTransform = false;
}

Colorwheel.prototype = {

  "getRGB": function() {
    return Snap.hsl2rgb(this.selectedHSL);
  },

  "create_hue_ring": function(mygroup, radius) {
    //console.log('create_hue_ring');
    degrees_per_segement = 360 / this.segments;
    degrees_per_segement =Math.ceil(degrees_per_segement);
    
    height  = 2*radius*Math.tan(Snap.rad(degrees_per_segement/2));
    for(var i=1; i<=this.segments; i++) {
      //draw single segment
      /*  circle 
        radius: 100
        segments: no_segments
        #°/segment: 360/no_segments
        height of outer end of segment: (approx) 2*radius*tan(seg°/2)
      */
      

      var gradient =this.mysnap.gradient('l(0,0.25, 0,0.75)'
          +Snap.hsl(i*degrees_per_segement,100,50)+
          ":00-"+Snap.hsl((i+1)*degrees_per_segement,100,50) +":100"); 

      mygroup.add(this.mysnap.path('m 0,0 l '+radius*0.95 +',-'+height/2 +'l 0,' +height +' z ').attr({
        fill:gradient,
        'fill-opacity':1,
        stroke:'#000',
        'stroke-width': '0',
        transform: new Snap.Matrix().rotate(i*degrees_per_segement,0,0)
        }));  
    }
    gradient =this.mysnap.gradient("r(0.5,0.5, 0.5, 0.5,0.5)rgba(0,0,0,1):10-rgba(0,0,0,0):60-rgba(255,255,255,0):70-rgba(255,255,255,1):95");

    mygroup.add(this.mysnap.circle(0,0,radius).attr({
        color:'#000000',
        fill:gradient,
        'fill-opacity':1,
        stroke:'#000',
        'stroke-width': '0'
        }));  

  },

  "create_bw_circle": function(mygroup) {
    mygroup.add(this.mysnap.circle(0,0,20).attr({
        
        fill:'#000000'
        }));    

  },

  "create": function(color_wheel_size) {
    var radius = color_wheel_size/2;

    this.topgroup = this.mysnap.g().attr({id:'cw-parent',transform: new Snap.Matrix().translate(radius,radius)});

    var mygroup = this.topgroup.g().attr({id:'colorwheel'});

    this.create_hue_ring(mygroup, radius);


    // color selection cirlce
    this.cs = this.mysnap.circle(0,-radius/2,20).attr({
        color:'#000000',
        fill:Snap.hsl(270,100,50),
        'fill-opacity':1,
        stroke:'#111',
        'stroke-width': 3,
        'stroke-opacity': 1,
        id: 'selected_color'
        });
    this.topgroup.add(this.cs);

    csRGBarray = this.cs.attr('fill').slice(4,-1).split(",");

    this.selectedHSL = Snap.rgb2hsl(parseInt(csRGBarray[0]),parseInt(csRGBarray[1]), parseInt(csRGBarray[2]));

    this.create_bw_circle(mygroup);

    this.cw = Snap('#colorwheel');
    //console.log(this);
    this.setupGlobalEventHandlers();
    this.close();
  },

  "close": function() {
    this.isOpen = false;
    this.cw.attr({visibility: 'hidden'});
    this.topgroup.attr({transform: new Snap.Matrix().translate(25,125)});
    $(this.mysnap.node).css({height: 50, width: 50}); 
  },

  "open": function() {
    this.isOpen = true;
    this.cw.attr({visibility: 'visible'});
    this.topgroup.attr({transform: new Snap.Matrix().translate(200,200)});
    $(this.mysnap.node).css({height: 400, width: 400}); 
  },

  "onPan": function(ev) {
    if(event instanceof MouseEvent) return false;
    //console.log('pan');
    if(this.isOpen) {
      this.transform.translateX = ev.deltaX;
      this.transform.translateY = ev.deltaY;

      this.requestUpdate();
    } else {
      this.open();
    }
  },

  "onRotate": function (ev) {
    if(event instanceof MouseEvent) return false;
    if(this.isOpen) {
      this.transform.angle = ev.rotation;

      this.requestUpdate();
    } else {
      this.open();
    }
  },

  "onPinch": function (ev) {
    if(event instanceof MouseEvent) return false;
    if (!this.isOpen) return true;
    this.scale(ev.scale);
  },

  "onTouchEnd": function (ev) {
    if(event instanceof MouseEvent) return false;
    if(this.isOpen) {
      this.updateElement();
    } 
  },

  "onTouchStart": function (ev) {
    if(event instanceof MouseEvent) return false;
    if(this.isOpen) {
      var t = this.cw.transform();
      var initial = TransformSplit(t.string);

      this.transform = {
        initial: initial,
        translateX: 0,
        translateY: 0,
        scale: 1,
        angle: 0,
        rx: 0,
        ry: 0,
        rz: 0
      };

      this.parentTransform = this.topgroup.matrix;
    }
  },

  "onTap": function(ev) {
    if(event instanceof MouseEvent) return false;
    if(this.isOpen) {
      this.close();
    }
    else {
      this.open();
    }
  },

  "requestUpdate": function() {
      requestAnimFrame(this.updateElement.bind(this));
  },

  "updateElement": function () {
    var t = this.transform;
    
    var h = this.selectedHSL.h;
    var hDeg = 360 * h;

    var hDeg = 270-(t.angle + t.initial.angle);
    this.selectedHSL.h = ((hDeg+360)%360)/360;

    var localY = t.translateY + t.initial.translateY;
    var radius = this.cw.getBBox().height/2;

    $("#output").load(radius);
    if(localY< -radius/2) {
      localY = -radius/2;
    } else if(localY > radius/2) {
      localY=radius/2;
    }

    var l = (radius/4+localY)/(radius/1.5);
    l = Math.min(Math.max(l, 0), 1);
    this.selectedHSL.l = l;

    this.cs.attr('fill', Snap.hsl2rgb(this.selectedHSL));

    var tstr = "t" + 0 + "," + localY
    + "s" + (t.scale * t.initial.scaleX)
    + "r" + (t.angle + t.initial.angle);
    this.cw.transform(tstr);
  },

  scale: function(scale, noEvent) {
    if(noEvent) {
      this.parentTransform = this.topgroup.matrix;
    }
    var t = new Snap.Matrix();

    var mat = new Snap.Matrix().scale(scale,scale,-this.cwsize/2,-this.cwsize/2);
    t.add(this.parentTransform).add(mat);
    this.topgroup.transform(t);
    var bbox = this.topgroup.getBBox();
    $(this.mysnap.node).css({height: bbox.h, width: bbox.w}); 
  },

  "setupGlobalEventHandlers": function () {
      var mc = new Hammer.Manager(this.mysnap.node);

      mc.add(new Hammer.Pan({ threshold: 5, pointers: 0 }));

      mc.add(new Hammer.Swipe()).recognizeWith(mc.get('pan'));
      mc.add(new Hammer.Rotate({ threshold: 0 })).recognizeWith(mc.get('pan'));
      mc.add(new Hammer.Pinch({ threshold: 0 })).recognizeWith([mc.get('pan'), mc.get('rotate')]);
      mc.add(new Hammer.Tap({threshold: 5, time: 1000}));

      mc.on("panstart panmove", this.onPan.bind(this));
      mc.on("rotatestart rotatemove", this.onRotate.bind(this));
      mc.on("pinchstart pinchmove", this.onPinch.bind(this));
      //mc.on("swipe", this.onSwipe.bind(this));
      mc.on("tap",this.onTap.bind(this));

      mc.on("hammer.input", function (ev) {
        if(ev.isFirst) {
          this.onTouchStart(ev);
        } else if(ev.isFinal) {
          this.onTouchEnd(ev);
        }
      }.bind(this));

      this.manager = mc;
  },

  "getSelectedColor": function() {
    return this.selectedHSL;
  }
};